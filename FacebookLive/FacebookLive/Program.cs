﻿using FacebookLive.Views;
using log4net;
using System;
using System.Windows.Forms;

namespace FacebookLive
{
    static class Program
    {
        /// <summary>
        /// Logger.
        /// </summary>
        public static readonly ILog Log = LogManager.GetLogger(typeof(Program));

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();

            Log.Info("Startup");
            Application.ApplicationExit += (sender, e) => Log.Info("Shutdown");

            AppDomain.CurrentDomain.UnhandledException += (sender, e) => HandleException(e.ExceptionObject);
            Application.ThreadException += (sender, e) => HandleException(e.Exception);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.Run(new MainWindow());
        }

        /// <summary>
        /// Called when an unhandled error occurs.
        /// </summary>
        /// <param name="sender">Caller.</param>
        /// <param name="e">Arguments passed in the call.</param>
        public static void HandleException(object exception)
        {
            if (exception is Exception e)
            {
                Log.Error(e.Message, e);
                throw e;
            }

            var message = $"Unhandled exception doesn't derive from System.Exception: {exception}";
            Log.Error(message);
        }
    }
}
