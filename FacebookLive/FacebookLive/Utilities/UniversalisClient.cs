﻿using FacebookLive.Models;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace FacebookLive.Utilities
{
    public class UniversalisClient
    {
        private readonly string _callback;
        private readonly string _url;

        public UniversalisClient(string url, string callback)
        {
            _url = url;
            _callback = callback;
        }

        public async Task<string> GetDayName(DateTime? date = null)
        {
            if (date == null)
                date = DateTime.Now;

            using (var wc = new WebClient())
            {
                var url = _url.Replace("{{DATE}}", date.Value.ToString("yyyyMMdd"));

                string jsonp;
                try
                {
                    jsonp = await wc.DownloadStringTaskAsync(url);
                }
                catch (WebException exception)
                {
                    Program.Log.Error($"Could not get day name: {exception.Message}.\n{exception.ResponseBody()}");
                    return null;
                }

                if (string.IsNullOrWhiteSpace(jsonp))
                    return null;

                var json = jsonp.Substring(_callback.Length + 1, jsonp.Length - _callback.Length - 4);

                return JsonConvert.DeserializeObject<UniversalisMassInfo>(json)?.DayName;
            }
        }
    }
}
