﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacebookLive.Utilities
{
    static class ControlExtensions
    {
        public static void CentreHorizontally(this Control control)
        {
            control.Left = (control.Parent.Width / 2) - (control.Width / 2);
        }

        public static async void HideAfterDelay(this Control control, TimeSpan delay)
        {
            await Task.Delay(delay);
            control.Visible = false;
        }

        public static void ShowError(this Control control, string message)
        {
            control.Text = $"⚠️ {message}";
            control.CentreHorizontally();
            control.Visible = true;
        }
    }
}
