﻿using System;

namespace FacebookLive.Utilities
{
    public static class TimeSpanConstants
    {
        public static readonly TimeSpan ONE_SECOND = TimeSpan.FromSeconds(1);
        public static readonly TimeSpan FOUR_SECONDS = TimeSpan.FromSeconds(4);
        public static readonly TimeSpan TEN_SECONDS = TimeSpan.FromSeconds(10);
        public static readonly TimeSpan ONE_MINUTE = TimeSpan.FromMinutes(1);
        public static readonly TimeSpan FIVE_MINUTES = TimeSpan.FromMinutes(5);
        public static readonly TimeSpan TEN_MINUTES = TimeSpan.FromMinutes(10);
        public static readonly TimeSpan THIRTY_MINUTES = TimeSpan.FromMinutes(30);
        public static readonly TimeSpan FORTY_MINUTES = TimeSpan.FromMinutes(40);
        public static readonly TimeSpan ONE_DAY = TimeSpan.FromDays(1);
    }
}
