﻿using FacebookLive.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FacebookLive.Utilities
{
    public static class MassTimesUtilities
    {
        // Allow matches before or after the current time
        private static TimeSpan VALID_INTERVAL = new TimeSpan(0, 55, 0);

        private static Dictionary<string, IEnumerable<MassTime>> _massTimes;

        private static Dictionary<string, IEnumerable<MassTime>> MassTimes
        {
            get
            {
                if (_massTimes == null)
                    _massTimes = new Dictionary<string, IEnumerable<MassTime>>
                    {
                        { "Sunday", MassTime.ParseList(Properties.Settings.Default.MassTimesSunday) },
                        { "Monday", MassTime.ParseList(Properties.Settings.Default.MassTimesMonday) },
                        { "Tuesday", MassTime.ParseList(Properties.Settings.Default.MassTimesTuesday) },
                        { "Wednesday", MassTime.ParseList(Properties.Settings.Default.MassTimesWednesday) },
                        { "Thursday", MassTime.ParseList(Properties.Settings.Default.MassTimesThursday) },
                        { "Friday", MassTime.ParseList(Properties.Settings.Default.MassTimesFriday) },
                        { "Saturday", MassTime.ParseList(Properties.Settings.Default.MassTimesSaturday) }
                    };

                return _massTimes;
            }
        }

        public static MassTime NearestMassTime()
        {
            var now = DateTime.Now;
            var today = now.ToString("dddd");
            var currentTime = now.TimeOfDay;
            var earliest = currentTime - VALID_INTERVAL;
            var latest = currentTime + VALID_INTERVAL;

            if (!MassTimes.ContainsKey(today))
                return null;

            foreach (var time in MassTimes[today])
            {
                if (time.Start >= earliest && time.Start <= latest)
                    return time;
            }

            return null;
        }

        public static MassTime NextMassTime()
        {
            var now = DateTime.Now;
            var earliest = now.TimeOfDay - VALID_INTERVAL;

            var initialKey = now.ToString("dddd");
            if (MassTimes.ContainsKey(initialKey))
            {
                var time = NextMassTimeImpl(MassTimes[initialKey], earliest);
                if (time != null)
                    return time;
            }

            string key = initialKey;
            int futureDays = 1;
            do
            {
                if (MassTimes.ContainsKey(key) && MassTimes[key].Count() > 0)
                    return MassTimes[key].First().Add(TimeSpan.FromDays(futureDays));

                futureDays++;
                key = now.AddDays(futureDays).ToString("dddd");
            } while (key != initialKey);

            return null;
        }

        private static MassTime NextMassTimeImpl(IEnumerable<MassTime> massTimes, TimeSpan earliest)
        {
            foreach (var time in massTimes)
            {
                if (time.Start >= earliest)
                    return time;
            }

            return null;
        }
    }
}
