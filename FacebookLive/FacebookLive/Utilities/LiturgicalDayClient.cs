﻿using System.Net;
using System.Threading.Tasks;

namespace FacebookLive.Utilities
{
    class LiturgicalDayClient
    {
        private readonly string _url;

        public LiturgicalDayClient(string url)
        {
            _url = url;
        }

        public async Task<string> GetDayName()
        {
            using (var wc = new WebClient())
            {
                try
                {
                    return await wc.DownloadStringTaskAsync(_url);
                }
                catch (WebException exception)
                {
                    Program.Log.Error($"Could not get day name: {exception.Message}.\n{exception.ResponseBody()}");
                    return null;
                }
            }
        }
    }
}
