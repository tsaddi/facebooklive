﻿using FacebookLive.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FacebookLive.Utilities
{
    public class FacebookClient : IDisposable
    {
        private static readonly int TITLE_MAX_LENGTH = 254;

        private readonly string _baseUrl;
        private readonly string _accessToken;
        private readonly string _page;
        private readonly WebClient _wc;

        public FacebookClient(string baseUrl, string accessToken, string page)
        {
            _baseUrl = baseUrl;
            _accessToken = accessToken;
            _page = page;

            _wc = new WebClient();
        }

        public async Task<FacebookPageInfo> GetPageInfo()
        {
            string json;

            try
            {
                json = await _wc.DownloadStringTaskAsync($"{_baseUrl}v7.0/{_page}?fields=name,picture.type(large)&access_token={_accessToken}");
            }
            catch (WebException exception)
            {
                Program.Log.Error($"Could not load Facebook page information: {exception.Message}.\n{exception.ResponseBody()}");
                return null;
            }

            return JsonConvert.DeserializeObject<FacebookPageInfo>(json);
        }

        public async Task<FacebookLiveVideo> CreateStream(string description)
        {
            var encodedDescription = Uri.EscapeDataString(description);
            var encodedTitle = Uri.EscapeDataString(description.Length > TITLE_MAX_LENGTH ? description.Substring(0, TITLE_MAX_LENGTH - 1) + "…" : description);

            byte[] response;
            try
            {
                response = await _wc.UploadValuesTaskAsync($"{_baseUrl}{_page}/live_videos?status=LIVE_NOW&title={encodedTitle}&description={encodedDescription}&access_token={_accessToken}", new NameValueCollection());
            }
            catch (WebException exception)
            {
                Program.Log.Error($"Could not create stream: {exception.Message}.\n{exception.ResponseBody()}");
                return null;
            }

            var json = Encoding.Default.GetString(response);

            return JsonConvert.DeserializeObject<FacebookLiveVideo>(json);
        }

        public void Dispose()
        {
            _wc.Dispose();
        }
    }
}
