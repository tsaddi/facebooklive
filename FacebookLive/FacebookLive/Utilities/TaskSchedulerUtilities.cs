﻿using Microsoft.Win32.TaskScheduler;
using System;
using System.IO;
using System.Windows.Forms;

namespace FacebookLive.Utilities
{
    public static class TaskSchedulerUtilities
    {
        private static readonly string TASK_NAME_WAKE = "FacebookLiveWakeComputer";
        private static readonly string TASK_NAME_AFTER_WAKE = "FacebookLiveWakeComputer";
        private static readonly string TASK_COMMAND = "cmd.exe";
        private static readonly string TASK_ARGUMENTS = "/C exit";

        public static void CreateWakeTask(DateTime wakeAt)
        {
            TaskService.Instance.Execute(TASK_COMMAND)
                .WithArguments(TASK_ARGUMENTS)
                .InWorkingDirectory(Application.StartupPath)
                .Once()
                .Starting(wakeAt)
                .Ending(wakeAt.AddMinutes(1))
                .When
                .WakingToRun()
                .DeletingTaskAfter(TimeSpan.FromSeconds(1))
                .AsTask(TASK_NAME_WAKE);
        }

        public static void DeleteWakeTask()
        {
            TaskService.Instance.RootFolder.DeleteTask(TASK_NAME_WAKE, false);
        }

        public static void CreateAfterWakeTask()
        {
            var task = TaskService.Instance.NewTask();

            task.Triggers.Add(new EventTrigger
            {
                Subscription = @"<QueryList>
  <Query Id='0' Path='System'>
    <Select Path='System'>*[System[Provider[@Name='Microsoft-Windows-Power-Troubleshooter'] and (Level=4 or Level=0) and (EventID=1)]]</Select>
  </Query>
</QueryList>"
            });

            task.Actions.Add(new ExecAction(Application.ExecutablePath, null, Application.StartupPath));

            TaskService.Instance.RootFolder.RegisterTaskDefinition(TASK_NAME_AFTER_WAKE, task);
        }

        public static void DeleteAfterWakeTask()
        {
            TaskService.Instance.RootFolder.DeleteTask(TASK_NAME_AFTER_WAKE, false);
        }
    }
}
