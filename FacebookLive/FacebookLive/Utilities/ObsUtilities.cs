﻿using FacebookLive.Models;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Security;

namespace FacebookLive.Utilities
{
    public static class ObsUtilities
    {
        /// <exception cref="ObsException"></exception>
        public static void UpdateStreamKey(string configFilePath, string streamUrl)
        {
            if (string.IsNullOrWhiteSpace(streamUrl))
                throw new ObsException("The stream URL was blank.");

            var parts = streamUrl.Split(new char[] { '/' }).ToList();
            var key = parts.Last();
            parts.Remove(key);
            var server = string.Join("/", parts) + '/';

            dynamic json = JsonConvert.DeserializeObject(ReadFile(configFilePath));

            if (!(json.settings is object))
                throw new ObsException("Could not update the stream key in the OBS config file because it has unexpected contents.");

            json.settings.key = key;
            json.settings.server = server;

            WriteFile(configFilePath, JsonConvert.SerializeObject(json, Formatting.Indented));
        }

        /// <exception cref="ObsException"></exception>
        private static string ReadFile(string path)
        {
            try
            {
                return File.ReadAllText(path);
            }
            catch (ArgumentNullException)
            {
                //     path is null.
                throw new ObsException("The OBS config file path has not been specified.");
            }
            catch (ArgumentException)
            {
                //     path is a zero-length string, contains only white space, or contains one or more
                //     invalid characters as defined by System.IO.Path.InvalidPathChars.
                throw new ObsException("The OBS config file path has not been specified.");
            }
            catch (PathTooLongException)
            {
                //     The specified path, file name, or both exceed the system-defined maximum length.
                throw new ObsException("The OBS config file path is too long.");
            }
            catch (DirectoryNotFoundException)
            {
                //     The specified path is invalid (for example, it is on an unmapped drive).
                throw new ObsException("The OBS config file does not exist.");
            }
            catch (FileNotFoundException)
            {
                //     The file specified in path was not found.
                throw new ObsException("The OBS config file does not exist.");
            }
            catch (IOException)
            {
                //     An I/O error occurred while opening the file.
                throw new ObsException("An error ocurred while opening the OBS config file.");
            }
            catch (UnauthorizedAccessException)
            {
                //     path specified a file that is read-only. -or- This operation is not supported
                //     on the current platform. -or- path specified a directory. -or- The caller does
                //     not have the required permission.
                throw new ObsException("Access to the OBS config file was denied.");
            }
            catch (NotSupportedException)
            {
                //     path is in an invalid format.
                throw new ObsException("The OBS config file path is invalid.");
            }
            catch (SecurityException)
            {
                //     The caller does not have the required permission.
                throw new ObsException("Access to the OBS config file was denied.");
            }
        }

        /// <exception cref="ObsException"></exception>
        private static void WriteFile(string path, string contents)
        {
            try
            {
                File.WriteAllText(path, contents);
            }
            catch (ArgumentNullException)
            {
                //     path is null or contents is empty.
                throw new ObsException("The OBS config file path has not been specified.");
            }
            catch (ArgumentException)
            {
                //     path is a zero-length string, contains only white space, or contains one or more
                //     invalid characters as defined by System.IO.Path.InvalidPathChars.
                throw new ObsException("The OBS config file path has not been specified.");
            }
            catch (PathTooLongException)
            {
                //     The specified path, file name, or both exceed the system-defined maximum length.
                throw new ObsException("The OBS config file path is too long.");
            }
            catch (DirectoryNotFoundException)
            {
                //     The specified path is invalid (for example, it is on an unmapped drive).
                throw new ObsException("The OBS config file does not exist.");
            }
            catch (IOException)
            {
                //     An I/O error occurred while opening the file.
                throw new ObsException("An error ocurred while saving the OBS config file.");
            }
            catch (UnauthorizedAccessException)
            {
                //     path specified a file that is read-only. -or- This operation is not supported
                //     on the current platform. -or- path specified a directory. -or- The caller does
                //     not have the required permission.
            }
            catch (NotSupportedException)
            {
                //     path is in an invalid format.
                throw new ObsException("Access to the OBS config file was denied.");
            }
            catch (SecurityException)
            {
                //     The caller does not have the required permission.
                throw new ObsException("Access to the OBS config file was denied.");
            }
        }
    }
}
