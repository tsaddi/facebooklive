﻿using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace FacebookLive.Utilities
{
    public static class SystemUtilities
    {
        public static Process StartApplication(string filePath, string arguments = null)
        {
            return Process.Start(new ProcessStartInfo
            {
                FileName = filePath,
                WorkingDirectory = Path.GetDirectoryName(filePath),
                Arguments = arguments
            });
        }

        public static void RestartComputer()
        {
            ShutdownComputerImpl("/r /t 0 /f");
        }

        public static void ShutdownComputer()
        {
            ShutdownComputerImpl("/s /t 0 /f");
        }

        public static bool SleepComputer()
        {
            return Application.SetSuspendState(PowerState.Suspend, true, false);
        }

        private static void ShutdownComputerImpl(string arguments)
        {
            using (Process.Start(new ProcessStartInfo
            {
                FileName = "shutdown",
                Arguments = arguments,
                CreateNoWindow = true,
                UseShellExecute = false
            })) { };
        }
    }
}
