﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Net;

namespace FacebookLive.Utilities
{
    public static class GraphicsUtilities
    {
        // Based on https://stackoverflow.com/a/34480702
        public static Bitmap ClipToCircle(Bitmap original)
        {
            var width = Math.Min(original.Width, original.Height);

            var srcRegion = new RectangleF(
                (original.Width / 2) - (width / 2),
                (original.Height / 2) - (width / 2),
                width,
                width
            );

            var copy = new Bitmap(width, width);
            using (var g = Graphics.FromImage(copy))
            {
                var r = new RectangleF(0, 0, width, width);
                using (var path = new GraphicsPath())
                {
                    path.AddEllipse(r);

                    var unit = GraphicsUnit.Pixel;
                    g.SetClip(path);
                    g.DrawImage(original, copy.GetBounds(ref unit), srcRegion, unit);

                    return copy;
                }
            }
        }

        public static Bitmap FromUrl(string url)
        {
            var request = WebRequest.Create(url);

            using (var response = request.GetResponse())
            using (var responseStream = response.GetResponseStream())
                return new Bitmap(responseStream);
        }
    }
}
