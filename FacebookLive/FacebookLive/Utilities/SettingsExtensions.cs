﻿using FacebookLive.Models;
using FacebookLive.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacebookLive.Utilities
{
    public static class SettingsExtensions
    {
        internal static TimeSpan ApplicationStartStreamingDelay(this Settings settings)
        {
            return TimeSpan.FromSeconds(settings.ApplicationStartStreamingDelay);
        }

        internal static TimeSpan ApplicationStopStreamingDelay(this Settings settings)
        {
            return TimeSpan.FromSeconds(settings.ApplicationStopStreamingDelay);
        }

        internal static TimeSpan ComputerAutoShutdownDelay(this Settings settings)
        {
            return TimeSpan.FromMinutes(settings.ComputerAutoShutdownDelay);
        }

        internal static TimeSpan? VigilAfter(this Settings settings)
        {
            return MassTime.ParseTime(settings.VigilAfter);
        }
    }
}
