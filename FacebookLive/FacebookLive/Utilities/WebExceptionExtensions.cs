﻿using System.IO;
using System.Net;

namespace FacebookLive.Utilities
{
    public static class WebExceptionExtensions
    {
        public static string ResponseBody(this WebException exception)
        {
            if (exception?.Response == null)
                return null;

            using (var reader = new StreamReader(exception.Response.GetResponseStream()))
                return reader.ReadToEnd();
        }
    }
}
