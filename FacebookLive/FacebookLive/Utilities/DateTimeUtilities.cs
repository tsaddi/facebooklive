﻿using System;

namespace FacebookLive.Utilities
{
    public static class DateTimeUtilities
    {
        public static string FormatInterval(TimeSpan interval) {
            if (interval.TotalSeconds < 0)
                throw new ArgumentException("Interval may not be negative.", nameof(interval));

            if (interval.TotalDays > 1)
                return $"{interval.Days} {(interval.Days == 1 ? "day" : "days")}, {interval.Hours}:{interval.Minutes:D2}:{interval.Seconds:D2}";

            if (interval.Hours > 0)
                return $"{interval.Hours}:{interval.Minutes:D2}:{interval.Seconds:D2}";

            if (interval.Minutes > 0)
                return $"{interval.Minutes}:{interval.Seconds:D2}";

            return $"{interval.Seconds} seconds";
        }

        public static string FormatTimeOfDay(TimeSpan? time, string suffix = "")
        {
            if (!time.HasValue)
                return string.Empty;

            if (time.Value.Minutes == 0)
            {
                switch (time.Value.Hours)
                {
                    case 0:
                        return $"12 midnight{suffix}";
                    case 12:
                        return $"12 noon{suffix}";
                    default:
                        return FormatHours(time.Value) + FormatAmPm(time.Value) + suffix;
                }
            }
            else
            {
                return FormatHours(time.Value) + ":" + time.Value.ToString("mm") + FormatAmPm(time.Value) + suffix;
            }
        }

        private static string FormatAmPm(TimeSpan time)
        {
            return time.Hours < 12 ? "am" : "pm";
        }

        private static string FormatHours(TimeSpan time)
        {
            return (time.Hours > 12 ? time.Hours - 12 : time.Hours).ToString();
        }
    }
}
