﻿using System.Windows.Forms;

namespace FacebookLive.ViewModels
{
    public class MainWindowViewModel
    {
        public MainWindowViewModel(
            LinkLabel cancel,
            Button createStream,
            Label errorMessage,
            PictureBox fbLogo,
            Label instructions,
            PictureBox loading,
            PictureBox pageLogo,
            Label pageName,
            LinkLabel skip,
            TextBox streamDescription,
            Form window)
        {
            Cancel = cancel;
            CreateStream = createStream;
            ErrorMessage = errorMessage;
            FbLogo = fbLogo;
            Instructions = instructions;
            Loading = loading;
            PageLogo = pageLogo;
            PageName = pageName;
            Skip = skip;
            StreamDescription = streamDescription;
            Window = window;
        }

        public LinkLabel Cancel { get; }

        public Button CreateStream { get; }

        public Label ErrorMessage { get; }

        public PictureBox FbLogo { get; }

        public Label Instructions { get; }

        public PictureBox Loading { get; }

        public PictureBox PageLogo { get; }

        public Label PageName { get; }

        public LinkLabel Skip { get; }

        public TextBox StreamDescription { get; }

        public Form Window { get; }
    }
}
