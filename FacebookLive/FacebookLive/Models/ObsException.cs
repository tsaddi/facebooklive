﻿using System;

namespace FacebookLive.Models
{
    public class ObsException : Exception
    {
        public ObsException(string message)
            : base(message)
        {
        }

        public ObsException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
