﻿namespace FacebookLive.Models
{
    public class FacebookPictureData
    {
        public string Width { get; set; }
        public string Height { get; set; }
        public string IsSilhouette { get; set; }
        public string Url { get; set; }
    }
}
