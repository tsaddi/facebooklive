﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace FacebookLive.Models
{
    public class MassTime
    {
        public TimeSpan Start { get; set; }

        public TimeSpan? End { get; set; }

        public MassTime Add(TimeSpan span) => new MassTime
        {
            Start = Start.Add(span),
            End = End?.Add(span)
        };

        public static IEnumerable<MassTime> ParseList(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
                return new List<MassTime>();

            return str.Split(',').Select(r =>
            {
                var times = r.Split(new char[] { '-' }, 2);
                if (times.Length != 2)
                    throw new Exception($"Invalid time range in configuration: {r}");

                return new MassTime
                {
                    Start = ParseTime(times[0]).Value,
                    End = ParseTime(times[1])
                };
            });
        }

        public static TimeSpan? ParseTime(string str)
        {
            if (string.IsNullOrEmpty(str))
                return null;

            var c = str.Split(new char[] { ':' }, 2);

            if (c.Length == 2 && int.TryParse(c[0], out int hour) && int.TryParse(c[1], out int minute))
                return new TimeSpan(hour, minute, 0);

            throw new Exception($"Invalid time in configuration: {str}");
        }
    }
}
