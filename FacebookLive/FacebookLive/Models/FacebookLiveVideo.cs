﻿namespace FacebookLive.Models
{
    public class FacebookLiveVideo
    {
        public string Id { get; set; }

        public string Secure_stream_url { get; set; }
    }
}
