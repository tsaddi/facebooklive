﻿namespace FacebookLive.Models
{
    public class FacebookPageInfo
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public FacebookPicture Picture { get; set; }
    }
}
