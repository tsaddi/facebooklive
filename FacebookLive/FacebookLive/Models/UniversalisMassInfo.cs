﻿using System.Collections.Generic;
using System.Net;
using System.Text.RegularExpressions;

namespace FacebookLive.Models
{
    class UniversalisMassInfo
    {
        private static readonly string PATTERN = "<b>([^<]+)</b>";
        private static readonly List<string> DAYS_OF_WEEK = new List<string> {
            "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
        };
        private static readonly List<string> SPECIAL_WEEKDAY_PREFIXES = new List<string> {
            "Divine", "Easter"
        };
        private static readonly List<string> SPECIAL_FEAST_PREFIXES = new List<string> {
            "Pentecost"
        };

        public string Day { get; set; }

        public string DayName
        {
            get
            {
                if (Day == null)
                    return null;

                var match = Regex.Match(Day, PATTERN);
                if (!match.Success)
                    return null;

                var name = WebUtility.HtmlDecode(match.Groups[1].Value);

                if (Day.IndexOf("-&#160;Solemnity") != -1)
                    return $"the solemnity of {name}";
                else if (Day.IndexOf("-&#160;Feast") != -1 || IsSpecialFeast(name))
                    return $"the feast of {name}";
                else if (StartsWithWeekday(name) || IsSpecialWeekday(name))
                    return name;
                else if (StartsWithNumber(name))
                    return $"the {name}";
                else
                    return $"the memorial of {name}";
            }
        }

        private bool StartsWithNumber(string str)
        {
            if (str == null)
                return false;

            return char.IsNumber(str[0]);
        }

        private bool StartsWithWeekday(string str)
        {
            return StartsWith(str, DAYS_OF_WEEK);
        }

        private bool IsSpecialWeekday(string str)
        {
            return StartsWith(str, SPECIAL_WEEKDAY_PREFIXES);
        }

        private bool IsSpecialFeast(string str)
        {
            return StartsWith(str, SPECIAL_FEAST_PREFIXES);
        }

        private bool StartsWith(string str, List<string> prefixes)
        {
            if (str == null)
                return false;

            var firstWord = str.Split(new char[] { ' ' }, 2)[0];
            return prefixes.Contains(firstWord);
        }
    }
}
