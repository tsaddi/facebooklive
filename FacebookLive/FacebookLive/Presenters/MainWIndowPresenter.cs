﻿using FacebookLive.Models;
using FacebookLive.Properties;
using FacebookLive.Utilities;
using static FacebookLive.Utilities.TimeSpanConstants;
using FacebookLive.ViewModels;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacebookLive.Presenters
{
    public class MainWindowPresenter
    {
        private static readonly Settings _settings = Settings.Default;

        private readonly MainWindowViewModel _vm;
        private readonly string _defaultInstructions;

        private bool _applicationStarted = false;
        private bool _countdownCancelled = false;
        private bool _textChanged = false;

        public MainWindowPresenter(MainWindowViewModel viewModel)
        {
            _vm = viewModel;
            _defaultInstructions = viewModel.Instructions.Text;

            TaskSchedulerUtilities.DeleteAfterWakeTask();
        }

        public void Init()
        {
            SetupUI();
            LoadPageInfo();
            LoadDayInfo();
            AutoStart();
        }

        private void ReInit()
        {
            if (_applicationStarted)
                return;

            if (_textChanged)
            {
                _textChanged = false;
                LoadDayInfo();
            }

            AutoStart();
        }

        public void CancelCountdown()
        {
            _countdownCancelled = true;
            HideCountdown();
        }

        public void CloseApplication()
        {
            TaskSchedulerUtilities.DeleteWakeTask();
            Application.Exit();
        }

        public async Task StartApplicationWithLiveStream()
        {
            ShowLoading();
            _countdownCancelled = true;

            if (await CreateLiveStream())
                StartApplicationAndShutdown(true, TimeSpan.FromHours(_settings.MaxStreamingHours));
            else
                HideLoading();
        }

        public void StartApplicationWithoutLiveStream()
        {
            ShowLoading();
            // We pass in a time after which to stop the application even though
            // we're not starting streaming, just to ensure that whatever service
            // is scheduled next actually gets run (we could get stuck with OBS
            // open and doing nothing otherwise).
            StartApplicationAndShutdown(false, TimeSpan.FromHours(_settings.MaxStreamingHours));
        }

        public void TextHasBeenChanged()
        {
            if (!_textChanged)
                HideLoading();

            _textChanged = true;
        }


        private async void AutoStart()
        {
            var nextMass = MassTimesUtilities.NextMassTime();
            if (nextMass == null)
            {
                await ShutdownAfterDelay();
                return;
            }

            var now = DateTime.Now.TimeOfDay;

            // Start streaming ten minutes before the Mass starts.
            var minimumStartAt = now.Add(TEN_SECONDS);  // Always give at least ten seconds to allow the user to cancel the auto start.
            var startAt = nextMass.Start.Subtract(TEN_MINUTES);
            if (startAt < minimumStartAt)
                startAt = minimumStartAt;

            var stopAt = nextMass.End?.Add(FIVE_MINUTES);  // Stop five minutes after Mass is due to finish.
            if (stopAt != null && stopAt <= now)
                return;

            var remainingToStart = startAt.Subtract(now);
            var remainingToEnd = stopAt?.Subtract(now) ?? TimeSpan.FromHours(_settings.MaxStreamingHours);

            // Shut down the computer if there are no more Masses scheduled today, and we're not due to start streaming imminently
            // (the latter could happen if a Mass is scheduled to start shortly after the computer is due to auto-start).
            if (remainingToStart > _settings.ComputerAutoShutdownDelay().Add(TEN_MINUTES))
            {
                var computerStartup = MassTime.ParseTime(_settings.ComputerStartupTime);
                if (computerStartup.HasValue)
                {
                    if (computerStartup < now)
                        computerStartup = computerStartup.Value.Add(ONE_DAY);

                    if (startAt > computerStartup)
                    {
                        await ShutdownAfterDelay();
                        return;
                    }
                }
            }

            // Sleep the computer, and schedule it to wake at the appropriate time,
            // if we're not due to start streaming soon.
            if (_settings.Sleep && remainingToStart > FORTY_MINUTES)
            {
                if (await SleepAndExitAfterDelay(DateTime.Now.Add(THIRTY_MINUTES), DateTime.Today.Add(startAt).Subtract(THIRTY_MINUTES)))
                    return;
            }

            await ShowCountdown(DateTime.Today.Add(startAt), "Streaming will start automatically in", "Starting streaming…");
            if (_countdownCancelled)
            {
                // After the cancelled event would have finished taking place, reset to
                // do whatever we would be doing if the app had only just been started.
                await Task.Delay(remainingToEnd);
                ReInit();
            }
            else
            {
                ShowLoading();
                if (await CreateLiveStream())
                    StartApplicationAndShutdown(true, remainingToEnd);
                else
                    HideLoading();
            }
        }

        private async Task AutoStop(Process process, TimeSpan stopAfter)
        {
            await Task.Delay(stopAfter);

            process.SendKeyPress(_settings.ApplicationStopStreamingHotKey);

            if (_settings.ApplicationStopStreamingDelay > 0)
                await Task.Delay(_settings.ApplicationStopStreamingDelay());

            process.CloseMainWindow();
        }

        private async Task<bool> CreateLiveStream(int retries = 0)
        {
            FacebookLiveVideo liveVideo;
            using (var client = new FacebookClient(
                _settings.FacebookGraphApiBaseUrl,
                _settings.FacebookAccessToken,
                _settings.FacebookPage
            ))
            {
                liveVideo = await client.CreateStream(_vm.StreamDescription.Text);
            }

            if (liveVideo == null)
            {
                Program.Log.Error("Could not create live stream with Facebook.");
                _vm.ErrorMessage.ShowError($"Could not create live stream with Facebook.");

                if (retries > _settings.MaxRetries)
                    return false;

                retries++;
                Program.Log.Error($"Retrying (attempt {retries} of {_settings.MaxRetries})...");

                await ShowCountdown(DateTime.Now.Add(ONE_MINUTE), "Retrying in", "Starting streaming…");
                if (_countdownCancelled)
                    return false;

                return await CreateLiveStream(retries);
            }

            try
            {
                ObsUtilities.UpdateStreamKey(_settings.ObsConfigPath, liveVideo.Secure_stream_url);
            }
            catch (ObsException exception)
            {
                Program.Log.Error("Could not create live stream", exception);
                _vm.ErrorMessage.ShowError($"Could not create live stream: {exception.Message}");
                return false;
            }

            return true;
        }

        private async void LoadDayInfo()
        {
            var client = new LiturgicalDayClient(_settings.LiturgicalDayUrl);
            var description = await client.GetDayName();
            if (string.IsNullOrWhiteSpace(description))
            {
                var date = DateTime.Now;
                var vigilPrefix = string.Empty;
                var dayPreposition = "on";

                // Saturday evening Masses are the vigil Mass of the Sunday.
                if (date.DayOfWeek == DayOfWeek.Saturday)
                {
                    var vigilAfter = _settings.VigilAfter();
                    if (vigilAfter.HasValue && date.TimeOfDay >= vigilAfter.Value)
                    {
                        date = date.AddDays(1);
                        dayPreposition = "of";
                        vigilPrefix = "Vigil ";
                    }
                }

                var universalis = new UniversalisClient(
                    _settings.UniversalisUrl,
                    _settings.UniversalisCallback
                );
                var dayName = await universalis.GetDayName(date);
                dayName = string.IsNullOrWhiteSpace(dayName) ? string.Empty : $" {dayPreposition} {dayName}";

                var massTime = DateTimeUtilities.FormatTimeOfDay(MassTimesUtilities.NearestMassTime()?.Start, " ");
                description = $"{massTime}{vigilPrefix}Mass{dayName}";
            }

            if (!_textChanged)
            {
                _vm.StreamDescription.Text = description;
                _textChanged = false;
                HideLoading();

                await Task.Delay(ONE_MINUTE);
                if (!_textChanged)
                    LoadDayInfo();
            }
        }

        private async void LoadPageInfo()
        {
            FacebookPageInfo pageInfo;
            using (var client = new FacebookClient(
                _settings.FacebookGraphApiBaseUrl,
                _settings.FacebookAccessToken,
                _settings.FacebookPage
            ))
            {
                pageInfo = await client.GetPageInfo();
            }

            if (pageInfo == null)
            {
                _vm.ErrorMessage.ShowError("Warning: Could not load Facebook page information (check that the internet connection is turned on).");
                _vm.StreamDescription.Visible = true;

                await Task.Delay(ONE_MINUTE);
                LoadPageInfo();

                return;
            }

            _vm.ErrorMessage.Visible = false;

            _vm.PageName.Text = pageInfo.Name;
            _vm.PageLogo.Image = GraphicsUtilities.ClipToCircle(GraphicsUtilities.FromUrl(pageInfo.Picture.Data.Url));

            var offset = _vm.PageName.Left - _vm.PageLogo.Left;
            var left = (_vm.Window.Width / 2) - ((_vm.PageName.Width + offset) / 2);
            _vm.PageLogo.Left = left;
            _vm.PageName.Left = left + offset;

            _vm.FbLogo.Visible = true;
            _vm.PageLogo.Visible = true;
            _vm.PageName.Visible = true;
            _vm.StreamDescription.Visible = true;
        }

        private void HideLoading()
        {
            _vm.Loading.Visible = false;
            _vm.CreateStream.Visible = true;
            _vm.Instructions.Visible = true;
            _vm.StreamDescription.Enabled = true;
        }

        private void SetupUI()
        {
            _vm.Loading.CentreHorizontally();
            _vm.StreamDescription.CentreHorizontally();
            _vm.CreateStream.CentreHorizontally();
            _vm.Instructions.CentreHorizontally();

            _vm.Skip.Text = $"Open {_settings.ApplicationName}{_vm.Skip.Text.Substring(8)}";

            // Enable transparency on Facebook logo
            _vm.PageLogo.Controls.Add(_vm.FbLogo);
            _vm.FbLogo.Location = new Point(_vm.PageLogo.Width - _vm.FbLogo.Width, _vm.PageLogo.Height - _vm.FbLogo.Height);

            _vm.CreateStream.Focus();
        }

        private async Task ShowCountdown(DateTime countdownTo, string prefix, string pleaseWait)
        {
            // Nudge the cancel link a little bit closer to the text, because it looks better.
            var cancelOffset = -3;

            _vm.Cancel.Visible = true;
            _countdownCancelled = false;

            var remaining = countdownTo.Subtract(DateTime.Now);
            while (remaining.TotalSeconds > 0)
            {
                if (_countdownCancelled)
                    return;

                _vm.Instructions.Text = $"{prefix} {DateTimeUtilities.FormatInterval(remaining)}.";
                _vm.Instructions.Left = (_vm.Window.Width / 2) - ((_vm.Instructions.Width + _vm.Cancel.Width + cancelOffset) / 2);
                _vm.Cancel.Left = _vm.Instructions.Left + _vm.Instructions.Width + cancelOffset;

                await Task.Delay(ONE_SECOND);
                remaining = countdownTo.Subtract(DateTime.Now);
            }

            _vm.Instructions.Text = pleaseWait;
        }

        private void HideCountdown()
        {
            _vm.Cancel.Visible = false;
            _vm.Instructions.Text = _defaultInstructions;
            _vm.Instructions.CentreHorizontally();
        }

        private void ShowLoading()
        {
            _vm.CreateStream.Visible = false;
            _vm.StreamDescription.Enabled = false;
            _vm.Instructions.Visible = false;
            _vm.Cancel.Visible = false;
            _vm.Loading.Visible = true;
        }

        private async Task ShutdownAfterDelay()
        {
            await ShowCountdown(
                DateTime.Now.Add(_settings.ComputerAutoShutdownDelay()),
                "No more services scheduled today. Computer will shut down automatically in",
                "Shutting down…"
            );

            if (_countdownCancelled)
            {
                // At the time the computer would have turned itself back on, do
                // whatever we would be doing if the app had only just been started.
                var computerStartup = MassTime.ParseTime(_settings.ComputerStartupTime);
                if (computerStartup.HasValue)
                {
                    var now = DateTime.Now.TimeOfDay;
                    if (computerStartup < now)
                        computerStartup = computerStartup.Value.Add(ONE_DAY);

                    var remainingUntilComputerStartup = computerStartup.Value - now;
                    await Task.Delay(remainingUntilComputerStartup);
                    ReInit();
                }
            }
            else
            {
                SystemUtilities.ShutdownComputer();
                Application.Exit();
            }
        }

        /// <summary>
        /// Puts the computer to sleep for a period, after showing a countdown timer.
        /// Also quits this application, after creating a scheduled task to re-run it
        /// when the computer wakes.
        /// </summary>
        /// <param name="wakeAt">When to wake from sleep.</param>
        /// <returns>
        /// true if the computer went to sleep, or
        /// false if the countdown timer was cancelled
        /// or if the computer refused to sleep.
        /// </returns>
        private async Task<bool> SleepAndExitAfterDelay(DateTime sleepAt, DateTime wakeAt)
        {
            await ShowCountdown(
                sleepAt,
                "No services scheduled until later today. Going to sleep in",
                "Going to sleep…"
            );

            if (_countdownCancelled)
            {
                HideCountdown();
                return false;
            }

            ShowLoading();

            TaskSchedulerUtilities.CreateWakeTask(wakeAt);
            if (SystemUtilities.SleepComputer())
            {
                TaskSchedulerUtilities.CreateAfterWakeTask();
                Program.Log.Info($"Shutting down due to sleep (due to wake at {wakeAt}).");
                Application.Exit();
                return true;
            }

            TaskSchedulerUtilities.DeleteWakeTask();

            HideCountdown();
            HideLoading();

            return false;
        }

        private async void StartApplicationAndShutdown(bool startStreaming, TimeSpan? stopAfter = null)
        {
            _vm.Window.HideAfterDelay(FOUR_SECONDS);
            _applicationStarted = true;

            using (var process = SystemUtilities.StartApplication(
                _settings.ApplicationExecutablePath,
                startStreaming ? _settings.ApplicationArgumentsStreaming : _settings.ApplicationArguments
            ))
            {
                if (startStreaming)
                {
                    await Task.Delay(_settings.ApplicationStartStreamingDelay());
                    process.SendKeyPress(_settings.ApplicationStartStreamingHotKey);
                }

                if (stopAfter.HasValue)
                {
                    await AutoStop(process, stopAfter.Value);

                    // Kill the application if it doesn't exit after one minute
                    // (this probably means that there's a confirmation dialog showing)
                    if (!process.WaitForExit(ONE_MINUTE.Milliseconds))
                    {
                        try
                        {
                            process.Kill();
                        }
                        catch (InvalidOperationException exception)
                        {
                            Program.Log.Info("Attempted to kill process, but it has already exited.", exception);
                        }
                    }
                }
                else
                {
                    await process.WaitForExitAsync();
                }

                // Restart the computer (rather than shutting down) if there are more Masses scheduled today.
                var nextMass = MassTimesUtilities.NextMassTime();
                var computerStartup = MassTime.ParseTime(_settings.ComputerStartupTime);
                if (nextMass != null && computerStartup.HasValue)
                {
                    if (computerStartup < DateTime.Now.TimeOfDay)
                        computerStartup = computerStartup.Value.Add(ONE_DAY);

                    if (nextMass.Start.Subtract(TEN_MINUTES) <= computerStartup)
                    {
                        Program.Log.Info("Restarting");
                        SystemUtilities.RestartComputer();
                        return;
                    }
                }

                SystemUtilities.ShutdownComputer();
                Application.Exit();
            };
        }
    }
}
