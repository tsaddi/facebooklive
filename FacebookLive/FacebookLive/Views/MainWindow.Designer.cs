﻿namespace FacebookLive.Views
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CloseButton = new System.Windows.Forms.Button();
            this.PageName = new System.Windows.Forms.Label();
            this.StreamDescription = new System.Windows.Forms.TextBox();
            this.CreateStream = new System.Windows.Forms.Button();
            this.Instructions = new System.Windows.Forms.Label();
            this.Skip = new System.Windows.Forms.LinkLabel();
            this.Loading = new System.Windows.Forms.PictureBox();
            this.FbLogo = new System.Windows.Forms.PictureBox();
            this.PageLogo = new System.Windows.Forms.PictureBox();
            this.ErrorMessage = new System.Windows.Forms.Label();
            this.Cancel = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.Loading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FbLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PageLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // CloseButton
            // 
            this.CloseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseButton.FlatAppearance.BorderSize = 0;
            this.CloseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CloseButton.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CloseButton.Location = new System.Drawing.Point(1233, -7);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(53, 38);
            this.CloseButton.TabIndex = 0;
            this.CloseButton.Text = "×";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.Close_Click);
            // 
            // PageName
            // 
            this.PageName.AutoSize = true;
            this.PageName.Font = new System.Drawing.Font("Segoe UI Light", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PageName.Location = new System.Drawing.Point(506, 90);
            this.PageName.Name = "PageName";
            this.PageName.Size = new System.Drawing.Size(60, 65);
            this.PageName.TabIndex = 3;
            this.PageName.Text = "…";
            this.PageName.Visible = false;
            // 
            // StreamDescription
            // 
            this.StreamDescription.AcceptsReturn = true;
            this.StreamDescription.Font = new System.Drawing.Font("Segoe UI Semilight", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StreamDescription.Location = new System.Drawing.Point(252, 383);
            this.StreamDescription.Multiline = true;
            this.StreamDescription.Name = "StreamDescription";
            this.StreamDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.StreamDescription.Size = new System.Drawing.Size(946, 155);
            this.StreamDescription.TabIndex = 4;
            this.StreamDescription.Text = "Mass";
            this.StreamDescription.Visible = false;
            // 
            // CreateStream
            // 
            this.CreateStream.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CreateStream.BackColor = System.Drawing.SystemColors.Highlight;
            this.CreateStream.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreateStream.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.CreateStream.Location = new System.Drawing.Point(581, 602);
            this.CreateStream.Name = "CreateStream";
            this.CreateStream.Size = new System.Drawing.Size(289, 45);
            this.CreateStream.TabIndex = 5;
            this.CreateStream.Text = "Start Streaming";
            this.CreateStream.UseVisualStyleBackColor = false;
            this.CreateStream.Visible = false;
            this.CreateStream.Click += new System.EventHandler(this.CreateStream_Click);
            // 
            // Instructions
            // 
            this.Instructions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Instructions.AutoSize = true;
            this.Instructions.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Instructions.Location = new System.Drawing.Point(572, 671);
            this.Instructions.Name = "Instructions";
            this.Instructions.Size = new System.Drawing.Size(258, 21);
            this.Instructions.TabIndex = 6;
            this.Instructions.Text = "The stream will go live immediately.";
            this.Instructions.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Instructions.Visible = false;
            // 
            // Skip
            // 
            this.Skip.ActiveLinkColor = System.Drawing.SystemColors.ActiveCaption;
            this.Skip.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Skip.AutoSize = true;
            this.Skip.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Skip.LinkColor = System.Drawing.SystemColors.ActiveCaption;
            this.Skip.Location = new System.Drawing.Point(23, 826);
            this.Skip.Name = "Skip";
            this.Skip.Size = new System.Drawing.Size(268, 20);
            this.Skip.TabIndex = 8;
            this.Skip.TabStop = true;
            this.Skip.Text = "Continue without creating a live stream";
            this.Skip.VisitedLinkColor = System.Drawing.SystemColors.ActiveCaption;
            this.Skip.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Skip_LinkClicked);
            // 
            // Loading
            // 
            this.Loading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Loading.BackColor = System.Drawing.Color.Transparent;
            this.Loading.Image = global::FacebookLive.Properties.Resources.spinner;
            this.Loading.InitialImage = global::FacebookLive.Properties.Resources.spinner;
            this.Loading.Location = new System.Drawing.Point(693, 597);
            this.Loading.Name = "Loading";
            this.Loading.Size = new System.Drawing.Size(64, 64);
            this.Loading.TabIndex = 9;
            this.Loading.TabStop = false;
            // 
            // FbLogo
            // 
            this.FbLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FbLogo.BackColor = System.Drawing.Color.Transparent;
            this.FbLogo.Image = global::FacebookLive.Properties.Resources.f_logo_RGB_Hex_Blue_512;
            this.FbLogo.Location = new System.Drawing.Point(429, 162);
            this.FbLogo.Name = "FbLogo";
            this.FbLogo.Size = new System.Drawing.Size(41, 40);
            this.FbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.FbLogo.TabIndex = 1;
            this.FbLogo.TabStop = false;
            this.FbLogo.Visible = false;
            // 
            // PageLogo
            // 
            this.PageLogo.BackColor = System.Drawing.Color.Transparent;
            this.PageLogo.Location = new System.Drawing.Point(329, 62);
            this.PageLogo.Name = "PageLogo";
            this.PageLogo.Size = new System.Drawing.Size(141, 140);
            this.PageLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PageLogo.TabIndex = 2;
            this.PageLogo.TabStop = false;
            this.PageLogo.Visible = false;
            // 
            // ErrorMessage
            // 
            this.ErrorMessage.AutoSize = true;
            this.ErrorMessage.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ErrorMessage.ForeColor = System.Drawing.Color.OrangeRed;
            this.ErrorMessage.Location = new System.Drawing.Point(689, 272);
            this.ErrorMessage.Name = "ErrorMessage";
            this.ErrorMessage.Size = new System.Drawing.Size(38, 25);
            this.ErrorMessage.TabIndex = 10;
            this.ErrorMessage.Text = "⚠️ ";
            this.ErrorMessage.Visible = false;
            // 
            // Cancel
            // 
            this.Cancel.ActiveLinkColor = System.Drawing.SystemColors.ActiveCaption;
            this.Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Cancel.AutoSize = true;
            this.Cancel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cancel.LinkColor = System.Drawing.SystemColors.ActiveCaption;
            this.Cancel.Location = new System.Drawing.Point(826, 672);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(53, 20);
            this.Cancel.TabIndex = 11;
            this.Cancel.TabStop = true;
            this.Cancel.Text = "Cancel";
            this.Cancel.Visible = false;
            this.Cancel.VisitedLinkColor = System.Drawing.SystemColors.ActiveCaption;
            this.Cancel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Cancel_LinkClicked);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(1284, 865);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.ErrorMessage);
            this.Controls.Add(this.Loading);
            this.Controls.Add(this.Skip);
            this.Controls.Add(this.Instructions);
            this.Controls.Add(this.CreateStream);
            this.Controls.Add(this.StreamDescription);
            this.Controls.Add(this.PageName);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.FbLogo);
            this.Controls.Add(this.PageLogo);
            this.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "MainWindow";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Loading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FbLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PageLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.PictureBox FbLogo;
        private System.Windows.Forms.PictureBox PageLogo;
        private System.Windows.Forms.Label PageName;
        private System.Windows.Forms.TextBox StreamDescription;
        private System.Windows.Forms.Button CreateStream;
        private System.Windows.Forms.Label Instructions;
        private System.Windows.Forms.LinkLabel Skip;
        private System.Windows.Forms.PictureBox Loading;
        private System.Windows.Forms.Label ErrorMessage;
        private System.Windows.Forms.LinkLabel Cancel;
    }
}

