﻿using FacebookLive.Presenters;
using FacebookLive.ViewModels;
using System;
using System.Windows.Forms;

namespace FacebookLive.Views
{
    public partial class MainWindow : Form
    {
        private MainWindowPresenter _presenter;

        public MainWindow()
        {
            InitializeComponent();

            var viewModel = new MainWindowViewModel(
                cancel: Cancel,
                createStream: CreateStream,
                errorMessage: ErrorMessage,
                fbLogo: FbLogo,
                instructions: Instructions,
                loading: Loading,
                pageLogo: PageLogo,
                pageName: PageName,
                skip: Skip,
                streamDescription: StreamDescription,
                window: this
            );

            _presenter = new MainWindowPresenter(viewModel);
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            StreamDescription.TextChanged += StreamDescription_TextChanged;
            _presenter.Init();
        }

        private void Cancel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) => _presenter.CancelCountdown();

        private void Close_Click(object sender, EventArgs e) => _presenter.CloseApplication();

        private async void CreateStream_Click(object sender, EventArgs e) => await _presenter.StartApplicationWithLiveStream();

        private void Skip_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) => _presenter.StartApplicationWithoutLiveStream();

        private void StreamDescription_TextChanged(object sender, EventArgs e) => _presenter.TextHasBeenChanged();
    }
}
